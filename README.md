
Domain-driven design - Foosball Tournament app in Java (with Hibernate ORM)
===========================================================================

Journey through the DDD with Hibernate 5 ORM and MySQL, and all of that in Java 8.


# Overview

1. `Tournament`, and `Team` are Aggregate Roots.
2. `Match` is an entity which exists only in the context of `Tournament` Aggregate Root, as the collection of matches.
3. Use of explicit state modelling for `Tournament` and `Match` instead of State Pattern. It's more natural choice
in the terms of modelling, but it's harder to implement on the persistence layer. Thanks to "discriminators" feature
in ORMs, implementation isn't that hard, although the whole topic needs a separate space for discussion.
4. In-memory repositories separation from domain by object deep cloning. You can find more about the advantages and
motivation in https://bitbucket.org/bzolynski/codefactors/src in DomainDrivenDesign/InMemoryRepositoriesSeparation
example.
5. Hibernate ORM enforces to use default constructors for all entities, and getters and setters (although they can
be private as Hibernate uses reflection).


# Installation

1. Build MySQL container and create database.

       make build-java-foosball-mysql
       make start-java-foosball-mysql
       make enter-java-foosball-mysql-container
       mysql -uroot -ppassword
       CREATE DATABASE foosball;
       exit
       exit

2. Run `src/main/java/application/Application.java` (JDK 8 is required).


# Running

1. Start MySQL container.

       make start-java-foosball-mysql
       
2. Run `src/main/java/application/Application.java`.


# Endpoints.

Accessing URL: `http://127.0.0.1:8080`.

All endpoints are available in [postman_collection.json](postman_collection.json) file.
