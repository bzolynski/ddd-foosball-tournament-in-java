
build-java-foosball-mysql :
	docker run --name java-foosball-mysql -v ~/mysql:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=password -p 3306 -d mysql


start-java-foosball-mysql :
	docker start java-foosball-mysql


stop-java-foosball-mysql :
	docker stop java-foosball-mysql


enter-java-foosball-mysql-container :
	 docker exec -it java-foosball-mysql /bin/bash
