package foosball.application;

import foosball.domain.exception.*;
import foosball.domain.model.*;
import foosball.domain.repository.TournamentRepositoryInterface;
import foosball.domain.valueobject.MatchId;
import foosball.domain.repository.TeamRepositoryInterface;
import foosball.domain.valueobject.TeamId;
import foosball.domain.valueobject.TournamentId;
import foosball.infrastructure.exception.TournamentNotFoundException;

import java.util.ArrayList;
import java.util.List;

public class TournamentService {
    private TournamentRepositoryInterface tournamentRepository;
    private TeamRepositoryInterface teamRepository;

    public TournamentService(
            TournamentRepositoryInterface tournamentRepository,
            TeamRepositoryInterface teamRepository

    ) {
        this.tournamentRepository = tournamentRepository;
        this.teamRepository = teamRepository;
    }

    public TournamentInit createTournament() {
        return tournamentRepository.create();
    }

    public TournamentInProgress startTournament(int id)
            throws TournamentNotFoundException, TournamentStartException, InvalidMatchArgumentException {
        TournamentId tournamentId = new TournamentId(id);
        TournamentInit tournamentInit = tournamentRepository.getTournamentInInitStateById(tournamentId);
        return tournamentRepository.start(tournamentInit);
    }

    public TournamentInit getTournamentInInitStateById(int id) throws TournamentNotFoundException {
        return tournamentRepository.getTournamentInInitStateById(new TournamentId(id));
    }

    public TournamentInProgress getTournamentInProgressStateById(int tournamentId) throws TournamentNotFoundException {
        return tournamentRepository.getTournamentInProgressStateById(new TournamentId(tournamentId));
    }

    public TournamentFinished getTournamentInFinishedStateById(int id)
            throws TournamentNotFoundException
    {
        return tournamentRepository.getTournamentInFinishedStateById(new TournamentId(id));
    }

    public ArrayList<TournamentInit> findTournamentsInInitState() {
        return tournamentRepository.findTournamentsInInitState();
    }

    public ArrayList<TournamentInProgress> findTournamentsInProgressState() {
        return tournamentRepository.findTournamentsInProgressState();
    }

    public ArrayList<TournamentFinished> findTournamentsInFinishedState() {
        return tournamentRepository.findTournamentsInFinishedState();
    }

    public List<MatchInterface> getMatchesForTournamentInProgress(int tournamentId) throws TournamentNotFoundException {
        TournamentInProgress tournamentInProgress = tournamentRepository.getTournamentInProgressStateById(
                new TournamentId(tournamentId)
        );
        return tournamentInProgress.matches();
    }

    public Team createTeam(String name, String strikerName, String goalKeeperName)
            throws InvalidStrikerNameException, InvalidGoalKeeperNameException, InvalidTeamNameException {
        return teamRepository.create(name, strikerName, goalKeeperName);
    }

    public Team getTeamById(int teamId) throws TeamNotFoundException {
        return teamRepository.getById(new TeamId(teamId));
    }

    public TournamentInit addTeamToTournament(int tournamentId, int teamId)
            throws TournamentNotFoundException, DuplicatedTeamException, TeamNotFoundException {

        Team team = teamRepository.getById(new TeamId(teamId));

        return tournamentRepository.addTeam(new TournamentId(tournamentId), team);
    }

    public MatchFinished finishMatch(
            int tournamentId,
            int matchId,
            int firstTeamScore,
            int secondTeamScore
    ) throws TournamentNotFoundException, MatchNotFoundException {
        TournamentInProgress tournament = tournamentRepository.getTournamentInProgressStateById(
                new TournamentId(tournamentId)
        );
        return tournamentRepository.finishMatch(
                tournament,
                new MatchId(matchId),
                firstTeamScore,
                secondTeamScore
        );
    }

    public TournamentFinished finishTournament(int tournamentId)
            throws TournamentNotFoundException, TournamentFinishException
    {
        TournamentInProgress tournamentInProgress = tournamentRepository.getTournamentInProgressStateById(
                new TournamentId(tournamentId)
        );
        return tournamentRepository.finish(tournamentInProgress);
    }

    public Integer getWinnerTeamForTournament(int tournamentId) throws TournamentNotFoundException {
        TournamentFinished tournament = tournamentRepository.getTournamentInFinishedStateById(
                new TournamentId(tournamentId)
        );
        return tournament.getWinnerTeamId();
    }
}
