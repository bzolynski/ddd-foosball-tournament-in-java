package foosball.userinterface;

import foosball.application.TournamentService;
import foosball.domain.exception.*;
import foosball.domain.model.*;
import foosball.infrastructure.exception.TournamentNotFoundException;
import foosball.infrastructure.persistence.hibernate.TeamRepository;
import foosball.infrastructure.persistence.hibernate.TournamentRepository;
import foosball.userinterface.request.AddTeamToTournamentRequest;
import foosball.userinterface.request.FinishMatchRequest;
import foosball.userinterface.request.TournamentRequestBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.ArrayList;

@RestController
@RequestMapping("/tournaments")
public class TournamentController {

    @Autowired
    private TeamRepository teamRepository;

    @Autowired
    private TournamentRepository tournamentRepository;

    @GetMapping("/init/{tournamentId}")
    public ResponseEntity getTournamentInInitStateById(@PathVariable int tournamentId) throws BadRequestException {
        TournamentService tournamentService = instantiateTournamentService();

        try {
            TournamentInit tournament = tournamentService.getTournamentInInitStateById(tournamentId);
            return ResponseEntity.ok(tournament);
        } catch (TournamentNotFoundException e) {
            throw new BadRequestException("Tournament does not exist");
        }
    }

    @GetMapping("/progress/{tournamentId}")
    public ResponseEntity getTournamentInProgressStateById(@PathVariable int tournamentId) throws BadRequestException {
        TournamentService tournamentService = instantiateTournamentService();

        try {
            TournamentInProgress tournament = tournamentService.getTournamentInProgressStateById(tournamentId);
            return ResponseEntity.ok(tournament);
        } catch (TournamentNotFoundException e) {
            throw new BadRequestException("Tournament does not exist");
        }
    }

    @GetMapping("/finished/{tournamentId}")
    public ResponseEntity getTournamentInFinishStateById(@PathVariable int tournamentId) throws BadRequestException {
        TournamentService tournamentService = instantiateTournamentService();

        try {
            TournamentFinished tournament = tournamentService.getTournamentInFinishedStateById(tournamentId);
            return ResponseEntity.ok(tournament);
        } catch (TournamentNotFoundException e) {
            throw new BadRequestException("Tournament does not exist");
        }
    }

    @GetMapping("/init")
    public ResponseEntity findTournamentsInInitState() throws BadRequestException {
        TournamentService tournamentService = instantiateTournamentService();

        ArrayList<TournamentInit>tournaments = tournamentService.findTournamentsInInitState();
        return ResponseEntity.ok(tournaments);
    }

    @GetMapping("/progress")
    public ResponseEntity findTournamentsInProgressState() throws BadRequestException {
        TournamentService tournamentService = instantiateTournamentService();

        ArrayList<TournamentInProgress>tournaments = tournamentService.findTournamentsInProgressState();
        return ResponseEntity.ok(tournaments);
    }

    @GetMapping("/finished")
    public ResponseEntity findTournamentsInFinishedState() throws BadRequestException {
        TournamentService tournamentService = instantiateTournamentService();

        ArrayList<TournamentFinished>tournaments = tournamentService.findTournamentsInFinishedState();
        return ResponseEntity.ok(tournaments);
    }

    @PostMapping()
    public ResponseEntity createTournament() {
        TournamentService tournamentService = instantiateTournamentService();
        TournamentInit tournament = tournamentService.createTournament();
        return ResponseEntity.accepted().body(tournament);
    }

    @PatchMapping("start")
    public ResponseEntity startTournament(@Valid TournamentRequestBody body, Errors errors) {

        if (errors.hasErrors()) {
            return ResponseEntity.badRequest().body(errors.getFieldErrors());
        }

        TournamentService tournamentService = instantiateTournamentService();
        try {
            TournamentInProgress tournamentInProgress = tournamentService.startTournament(
                    body.getTournament()
            );
            return ResponseEntity.accepted().body(tournamentInProgress);
        } catch (TournamentNotFoundException e) {
            throw new BadRequestException("Tournament does not exist");
        } catch (TournamentStartException e) {
            throw new BadRequestException("Tournament cannot be started, at least 2 teams needed...");
        } catch (InvalidMatchArgumentException e) {
            throw new BadRequestException("Something went wrong with matches setup, cannot start tournament...");
        }
    }

    @PatchMapping("finish")
    public ResponseEntity finishTournament(@Valid TournamentRequestBody body, Errors errors) {

        if (errors.hasErrors()) {
            return ResponseEntity.badRequest().body(errors.getFieldErrors());
        }

        TournamentService tournamentService = instantiateTournamentService();
        try {
            TournamentFinished tournamentFinished = tournamentService.finishTournament(
                    body.getTournament()
            );
            return ResponseEntity.accepted().body(tournamentFinished);
        } catch (TournamentNotFoundException e) {
            throw new BadRequestException("Tournament does not exist");
        } catch (TournamentFinishException e) {
            throw new BadRequestException("Tournament cannot be finished, some matches are in progress...");
        }
    }

    @PatchMapping("teams")
    public ResponseEntity addTeamToTournament(@Valid AddTeamToTournamentRequest body, Errors errors) {

        if (errors.hasErrors()) {
            return ResponseEntity.badRequest().body(errors.getFieldErrors());
        }

        TournamentService tournamentService = instantiateTournamentService();
        try {
            TournamentInit tournamentInit = tournamentService.addTeamToTournament(
                    body.getTournament(), body.getTeam()
            );
            return ResponseEntity.accepted().body(tournamentInit);
        } catch (TournamentNotFoundException e) {
            throw new BadRequestException("Tournament does not exist");
        } catch (TeamNotFoundException e) {
            throw new BadRequestException("Team does not exist");
        } catch (DuplicatedTeamException e) {
            throw new BadRequestException("Team already added to this tournament");
        }
    }

    @PatchMapping()
    public ResponseEntity finishMatch(@Valid FinishMatchRequest body, Errors errors) {

        if (errors.hasErrors()) {
            return ResponseEntity.badRequest().body(errors.getFieldErrors());
        }

        TournamentService tournamentService = instantiateTournamentService();
        try {
            MatchFinished matchFinished = tournamentService.finishMatch(
                    body.getTournament(),
                    body.getMatch(),
                    body.getScore1(),
                    body.getScore2()
            );
            return ResponseEntity.accepted().body(matchFinished);
        } catch (TournamentNotFoundException e) {
            throw new BadRequestException("Tournament does not exist");
        } catch (MatchNotFoundException e) {
            throw new BadRequestException("Match does not exist");
        }
    }

    private TournamentService instantiateTournamentService() {
        return new TournamentService(
                tournamentRepository,
                teamRepository
        );
    }
}
