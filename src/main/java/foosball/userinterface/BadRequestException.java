package foosball.userinterface;

import java.util.List;

@SuppressWarnings("serial")
public class BadRequestException extends RuntimeException {

    private List<FieldErrorResource> errors;

    public BadRequestException(String message) {
        super(message);
    }

    public BadRequestException(String message, List<FieldErrorResource> errors) {
        super(message);
        this.errors = errors;
    }

    public List<FieldErrorResource> getFieldErrors() { return errors; }
}
