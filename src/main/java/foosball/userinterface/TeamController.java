package foosball.userinterface;

import foosball.application.TournamentService;
import foosball.domain.exception.InvalidGoalKeeperNameException;
import foosball.domain.exception.InvalidStrikerNameException;
import foosball.domain.exception.InvalidTeamNameException;
import foosball.domain.exception.TeamNotFoundException;
import foosball.domain.model.Team;
import foosball.infrastructure.persistence.hibernate.TeamRepository;
import foosball.infrastructure.persistence.hibernate.TournamentRepository;
import foosball.userinterface.request.TeamRequestBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/teams")
public class TeamController {

    @Autowired
    private TeamRepository teamRepository;

    @Autowired
    private TournamentRepository tournamentRepository;

    @GetMapping("{teamId}")
    public ResponseEntity getTeamById(@PathVariable int teamId) throws BadRequestException {
        TournamentService tournamentService = instantiateTournamentService();

        try {
            Team team = tournamentService.getTeamById(teamId);
            return ResponseEntity.ok(team);
        } catch (TeamNotFoundException e) {
            throw new BadRequestException("Team does not exist");
        }
    }

    @PostMapping()
    public ResponseEntity createTeam(@Valid TeamRequestBody body, Errors errors) {

        if (errors.hasErrors()) {
            return ResponseEntity.badRequest().body(errors.getFieldErrors());
        }

        try {
            TournamentService tournamentService = instantiateTournamentService();
            Team team = tournamentService.createTeam(body.getName(), body.getStriker(), body.getGoalkeeper());
            return ResponseEntity.accepted().body(team);
        } catch (InvalidTeamNameException e) {
            List<FieldErrorResource> fieldErrorResources = new ArrayList<>();
            fieldErrorResources.add(new FieldErrorResource(
                            "name",
                            "Team",
                            "INVALID_FIELD",
                            "Field is invalid"
                    )
            );
            throw new BadRequestException("Invalid team name", fieldErrorResources);
        } catch (InvalidStrikerNameException e) {
            List<FieldErrorResource> fieldErrorResources = new ArrayList<>();
            fieldErrorResources.add(new FieldErrorResource(
                            "striker",
                            "Striker",
                            "INVALID_FIELD",
                            "Field is invalid"
                    )
            );
            throw new BadRequestException("Invalid player name", fieldErrorResources);
        } catch (InvalidGoalKeeperNameException e) {
            List<FieldErrorResource> fieldErrorResources = new ArrayList<>();
            fieldErrorResources.add(new FieldErrorResource(
                            "goalkeeper",
                            "GoalKeeper",
                            "INVALID_FIELD",
                            "Field is invalid"
                    )
            );
            throw new BadRequestException("Invalid player name", fieldErrorResources);
        }
    }

    private TournamentService instantiateTournamentService() {
        return new TournamentService(
                tournamentRepository,
                teamRepository
        );
    }
}
