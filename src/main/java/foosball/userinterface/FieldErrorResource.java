package foosball.userinterface;

public class FieldErrorResource {
    private String field;
    private String resource;
    private String code;
    private String message;

    public FieldErrorResource(String field, String resource, String code, String message) {
        setField(field);
        setResource(resource);
        setCode(code);
        setMessage(message);
    }

    public String getResource() { return resource; }

    public void setResource(String resource) { this.resource = resource; }

    public String getField() { return field; }

    private void setField(String field) { this.field = field; }

    public String getCode() { return code; }

    private void setCode(String code) { this.code = code; }

    public String getMessage() { return message; }

    private void setMessage(String message) { this.message = message; }
}
