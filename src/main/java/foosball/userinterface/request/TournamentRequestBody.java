package foosball.userinterface.request;

import javax.validation.constraints.NotNull;

public class TournamentRequestBody {

    @NotNull(message = "Tournament id cannot be empty")
    private Integer id;

    public Integer getTournament() {
        return id;
    }

    public void setTournament(Integer id) {
        this.id = id;
    }
}
