package foosball.userinterface.request;

import javax.validation.constraints.NotNull;

public class AddTeamToTournamentRequest {

    @NotNull(message = "Tournament id cannot be empty")
    private Integer tournament;

    @NotNull(message = "Team id cannot be empty")
    private Integer team;

    public Integer getTournament() {
        return tournament;
    }

    public void setTournament(Integer tournament) {
        this.tournament = tournament;
    }

    public Integer getTeam() {
        return team;
    }

    public void setTeam(Integer team) {
        this.team = team;
    }
}
