package foosball.userinterface.request;

import javax.validation.constraints.NotNull;

public class FinishMatchRequest {

    @NotNull(message = "Tournament id cannot be empty")
    private Integer tournament;

    @NotNull(message = "Match id cannot be empty")
    private Integer match;

    @NotNull(message = "Score for team 1 cannot be empty")
    private Integer score1;

    @NotNull(message = "Score for team 2 cannot be empty")
    private Integer score2;

    public Integer getTournament() {
        return tournament;
    }

    public void setTournament(Integer tournament) {
        this.tournament = tournament;
    }

    public Integer getMatch() {
        return match;
    }

    public void setMatch(Integer match) {
        this.match = match;
    }

    public Integer getScore1() {
        return score1;
    }

    public void setScore1(Integer score1) {
        this.score1 = score1;
    }

    public Integer getScore2() {
        return score2;
    }

    public void setScore2(Integer score2) {
        this.score2 = score2;
    }
}
