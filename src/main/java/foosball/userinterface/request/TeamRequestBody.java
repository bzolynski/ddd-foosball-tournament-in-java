package foosball.userinterface.request;

import javax.validation.constraints.NotNull;

public class TeamRequestBody {

    @NotNull(message = "Name of team cannot be empty")
    private String name;

    @NotNull(message = "Name of striker cannot be empty")
    private String striker;

    @NotNull(message = "Name of goal keeper cannot be empty")
    private String goalkeeper;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getStriker() {
        return striker;
    }

    public void setStriker(String striker) {
        this.striker = striker;
    }

    public String getGoalkeeper() {
        return goalkeeper;
    }

    public void setGoalkeeper(String goalkeeper) {
        this.goalkeeper = goalkeeper;
    }
}
