package foosball.infrastructure.persistence.inmemory;

import foosball.domain.exception.*;
import foosball.domain.model.*;
import foosball.domain.repository.TournamentRepositoryInterface;
import foosball.domain.valueobject.MatchId;
import foosball.domain.shared.IdentityGenerator;
import foosball.domain.valueobject.TournamentId;
import foosball.infrastructure.exception.TournamentNotFoundException;

import java.util.ArrayList;

public class InMemoryTournamentRepository extends InMemoryRepository<Tournament>
        implements TournamentRepositoryInterface {

    public InMemoryTournamentRepository() {
        records = new ArrayList<>();
    }

    @Override
    public TournamentInit create() {
        int id = IdentityGenerator.get();
        TournamentId tournamentId = new TournamentId(id);
        TournamentInit tournament = new TournamentInit(tournamentId);
        return (TournamentInit)persist(tournament);
    }

    @Override
    public TournamentInProgress start(TournamentInit tournamentInit)
            throws TournamentNotFoundException, TournamentStartException, InvalidMatchArgumentException {
        TournamentInProgress tournamentInProgress = tournamentInit.start();
        return (TournamentInProgress)persist(tournamentInProgress);
    }

    @Override
    public TournamentFinished finish(TournamentInProgress tournamentInProgress) throws TournamentFinishException {
        TournamentFinished tournamentFinished = tournamentInProgress.finish();
        return (TournamentFinished)persist(tournamentFinished);
    }

    @Override
    public TournamentInit addTeam(TournamentId tournamentId, Team team)
        throws TournamentNotFoundException, DuplicatedTeamException {

        TournamentInit tournament = getTournamentInInitStateById(tournamentId);
        if (tournament == null) {
            throw new TournamentNotFoundException("No tournament found");
        }
        tournament.addTeam(deepCloneTeam(team));
        return (TournamentInit)persist(tournament);
    }

    @Override
    public TournamentInit getTournamentInInitStateById(TournamentId tournamentId) throws TournamentNotFoundException {
        if (records.isEmpty()) {
            throwTournamentNotFoundException(tournamentId);
        }
        for (TournamentInterface tournament: records) {
            if (tournamentId.equals(tournament.id())) {
                if (tournament instanceof TournamentInit) {
                    return (TournamentInit)deepCloneTournament(tournament);
                }
                throwTournamentNotFoundException(tournamentId);
            }
        }
        throwTournamentNotFoundException(tournamentId);
        return null;
    }

    @Override
    public TournamentInProgress getTournamentInProgressStateById(TournamentId tournamentId)
            throws TournamentNotFoundException {
        if (records.isEmpty()) {
            throwTournamentNotFoundException(tournamentId);
        }
        for (TournamentInterface tournament: records) {
            if (tournamentId.equals(tournament.id())) {
                if (tournament instanceof TournamentInProgress) {
                    return (TournamentInProgress)deepCloneTournament(tournament);
                }
                throwTournamentNotFoundException(tournamentId);
            }
        }
        throwTournamentNotFoundException(tournamentId);
        return null;
    }

    @Override
    public TournamentFinished getTournamentInFinishedStateById(TournamentId tournamentId)
            throws TournamentNotFoundException {
        if (records.isEmpty()) {
            throwTournamentNotFoundException(tournamentId);
        }
        for (TournamentInterface tournament: records) {
            if (tournamentId.equals(tournament.id())) {
                if (tournament instanceof TournamentFinished) {
                    return (TournamentFinished) deepCloneTournament(tournament);
                }
                throwTournamentNotFoundException(tournamentId);
            }
        }
        throwTournamentNotFoundException(tournamentId);
        return null;
    }

    @Override
    public ArrayList<TournamentInit> findTournamentsInInitState() {
        ArrayList<TournamentInit> tournamentsList = new ArrayList<>();
        if (records.isEmpty()) {
            return tournamentsList;
        }
        for (TournamentInterface tournament: records) {
            if (tournament instanceof TournamentInit) {
                tournamentsList.add(((TournamentInit)deepCloneTournament(tournament)));
            }
        }
        return tournamentsList;
    }

    @Override
    public ArrayList<TournamentInProgress> findTournamentsInProgressState() {
        ArrayList<TournamentInProgress> tournamentsList = new ArrayList<>();
        if (records.isEmpty()) {
            return tournamentsList;
        }
        for (TournamentInterface tournament: records) {
            if (tournament instanceof TournamentInProgress) {
                tournamentsList.add(((TournamentInProgress)deepCloneTournament(tournament)));
            }
        }
        return tournamentsList;
    }

    @Override
    public ArrayList<TournamentFinished> findTournamentsInFinishedState() {
        ArrayList<TournamentFinished> tournamentsList = new ArrayList<>();
        if (records.isEmpty()) {
            return tournamentsList;
        }
        for (TournamentInterface tournament: records) {
            if (tournament instanceof TournamentFinished) {
                tournamentsList.add(((TournamentFinished)deepCloneTournament(tournament)));
            }
        }
        return tournamentsList;
    }

    @Override
    public MatchFinished finishMatch(
            TournamentInProgress tournament,
            MatchId matchId,
            int firstTeamScore,
            int secondTeamScore
    ) throws MatchNotFoundException {
        MatchFinished matchFinished = tournament.setMatchResult(matchId, firstTeamScore, secondTeamScore);
        persist(tournament);
        return deepCloneMatchFinished(matchFinished);
    }

    private Team deepCloneTeam(Team object) {
        return getClonerOnce().deepClone(object);
    }

    private MatchFinished deepCloneMatchFinished(MatchFinished object) {
        return getClonerOnce().deepClone(object);
    }

    private TournamentInterface deepCloneTournament(TournamentInterface object) {
        return getClonerOnce().deepClone(object);
    }

    private void throwTournamentNotFoundException(TournamentId tournamentId) throws TournamentNotFoundException {
        throw new TournamentNotFoundException(
                "Tournament with id: " + String.valueOf(tournamentId.getValue()) + " not found"
        );
    }
}
