package foosball.infrastructure.persistence.inmemory;

import com.rits.cloning.Cloner;
import foosball.domain.shared.AggregateRoot;
import java.util.ArrayList;

abstract class InMemoryRepository<T extends AggregateRoot> {

    private static Cloner cloner;

    ArrayList<T> records;

    static Cloner getClonerOnce() {
        if (cloner == null) {
            cloner = new Cloner();
        }
        return cloner;
    }

    T persist(T record) {
        if (!records.isEmpty()) {
            for (int key = 0; key < records.size(); key++) {
                T aRecord = records.get(key);
                if (record.id().equals(aRecord.id())) {
                    // Update record
                    records.set(key, deepClone(record));
                    return deepClone(record);
                }
            }
        }

        // Insert record
        records.add(record);
        return deepClone(record);
    }

    // Deep cloning objects so they can be fully detached from the OOP references (in opposite to shallow cloning)
    // It simulates repository and domain separation
    // It's not necessarily needed though, but helps to distinguish all places,
    // where persistence needs to be performed (see: InMemoryTournamentRepository.persist())
    T deepClone(T object) {
        return getClonerOnce().deepClone(object);
    }
}
