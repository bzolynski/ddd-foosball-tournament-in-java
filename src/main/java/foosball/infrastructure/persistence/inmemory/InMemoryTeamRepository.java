package foosball.infrastructure.persistence.inmemory;

import foosball.domain.exception.InvalidGoalKeeperNameException;
import foosball.domain.exception.InvalidStrikerNameException;
import foosball.domain.shared.IdentityGenerator;
import foosball.domain.exception.InvalidTeamNameException;
import foosball.domain.exception.TeamNotFoundException;
import foosball.domain.model.TeamFactory;
import foosball.domain.model.Team;
import foosball.domain.repository.TeamRepositoryInterface;
import foosball.domain.valueobject.TeamId;

import java.util.ArrayList;

public class InMemoryTeamRepository extends InMemoryRepository<Team> implements TeamRepositoryInterface {

    public InMemoryTeamRepository() {
        records = new ArrayList<>();
    }

    public Team create(String name, String strikerName, String goalKeeperName)
            throws InvalidStrikerNameException, InvalidGoalKeeperNameException, InvalidTeamNameException {
        int id = IdentityGenerator.get();
        TeamFactory teamFactory = new TeamFactory();
        TeamId teamId = new TeamId(id);
        Team team = teamFactory.create(teamId, name, strikerName, goalKeeperName);
        return persist(team);
    }

    public Team getById(TeamId teamId) throws TeamNotFoundException {
        if (records.isEmpty()) {
            throwTeamNotFoundException(teamId);
        }
        for (Team team: records) {
            if (teamId.equals(team.id())) {
                return deepClone(team);
            }
        }
        throwTeamNotFoundException(teamId);
        return null;
    }

    private void throwTeamNotFoundException(TeamId teamId) throws TeamNotFoundException {
        throw new TeamNotFoundException(
                "Team with id: " + String.valueOf(teamId.getValue()) + " not found"
        );
    }
}
