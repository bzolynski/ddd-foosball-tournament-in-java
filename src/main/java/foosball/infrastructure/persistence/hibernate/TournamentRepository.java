package foosball.infrastructure.persistence.hibernate;

import foosball.domain.exception.*;
import foosball.domain.model.*;
import foosball.domain.repository.TournamentRepositoryInterface;
import foosball.domain.valueobject.MatchId;
import foosball.domain.valueobject.TournamentId;
import foosball.infrastructure.exception.TournamentNotFoundException;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.ArrayList;

@Repository
@Transactional
public class TournamentRepository implements TournamentRepositoryInterface {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public TournamentInit create() {
        int id = foosball.domain.shared.IdentityGenerator.get();
        TournamentId tournamentId = new TournamentId(id);
        TournamentInit tournament = new TournamentInit(tournamentId);
        entityManager.persist(tournament);
        return tournament;
    }

    @Override
    public TournamentInProgress start(TournamentInit tournamentInit)
            throws TournamentNotFoundException, TournamentStartException, InvalidMatchArgumentException {

        entityManager.remove(tournamentInit);

        TournamentInProgress tournamentInProgress = tournamentInit.start();
        entityManager.persist(tournamentInProgress);

        return tournamentInProgress;
    }

    @Override
    public TournamentFinished finish(TournamentInProgress tournamentInProgress) throws TournamentFinishException {
        TournamentFinished tournamentFinished = tournamentInProgress.finish();

        Query query = entityManager.createNativeQuery(
                "UPDATE tournaments SET state = :state WHERE id = :id"
        );
        query.setParameter("state", "finished");
        query.setParameter("id", tournamentFinished.id().getValue());
        query.executeUpdate();

        return tournamentFinished;
    }

    @Override
    public TournamentInit getTournamentInInitStateById(TournamentId tournamentId) throws TournamentNotFoundException {

        TournamentInit tournamentInit = entityManager.find(TournamentInit.class, tournamentId.getValue());
        if (tournamentInit == null) {
            throw new TournamentNotFoundException(
                    "Tournament with id: " + String.valueOf(tournamentId.getValue()) + " not found"
            );
        }
        return tournamentInit;
    }

    @Override
    public TournamentInProgress getTournamentInProgressStateById(TournamentId tournamentId)
            throws TournamentNotFoundException {

        TournamentInProgress tournamentInProgress = entityManager.find(
                TournamentInProgress.class, tournamentId.getValue()
        );
        if (tournamentInProgress == null) {
            throw new TournamentNotFoundException(
                    "Tournament with id: " + String.valueOf(tournamentId.getValue()) + " not found"
            );
        }
        return tournamentInProgress;
    }

    @Override
    public TournamentFinished getTournamentInFinishedStateById(TournamentId tournamentId)
            throws TournamentNotFoundException {

        TournamentFinished tournamentFinished = entityManager.find(
                TournamentFinished.class, tournamentId.getValue()
        );
        if (tournamentFinished == null) {
            throw new TournamentNotFoundException(
                    "Tournament with id: " + String.valueOf(tournamentId.getValue()) + " not found"
            );
        }
        return tournamentFinished;
    }

    @Override
    public ArrayList<TournamentInit> findTournamentsInInitState() {

        Query query = entityManager.createQuery("SELECT t FROM Tournament t WHERE state = 'init'");
        return (ArrayList<TournamentInit>) query.getResultList();
    }

    @Override
    public ArrayList<TournamentInProgress> findTournamentsInProgressState() {

        Query query = entityManager.createQuery("SELECT t FROM Tournament t WHERE state = 'progress'");
        return (ArrayList<TournamentInProgress>) query.getResultList();
    }

    @Override
    public ArrayList<TournamentFinished> findTournamentsInFinishedState() {

        Query query = entityManager.createQuery("SELECT t FROM Tournament t WHERE state = 'finished'");
        return (ArrayList<TournamentFinished>) query.getResultList();
    }

    @Override
    public TournamentInit addTeam(TournamentId tournamentId, Team team)
            throws TournamentNotFoundException, DuplicatedTeamException {

        TournamentInit tournamentInit = getTournamentInInitStateById(tournamentId);
        if (tournamentInit == null) {
            throw new TournamentNotFoundException("No tournament found");
        }
        tournamentInit.addTeam(team);
        entityManager.persist(tournamentInit);
        return tournamentInit;
    }

    @Override
    public MatchFinished finishMatch(
            TournamentInProgress tournament,
            MatchId matchId,
            int firstTeamScore,
            int secondTeamScore
    ) throws MatchNotFoundException {
        MatchFinished matchFinished = tournament.setMatchResult(matchId, firstTeamScore, secondTeamScore);

        entityManager.clear();

        Query query = entityManager.createNativeQuery(
                "DELETE FROM matches_collections WHERE id = :match_id AND tournament_id = :tournament_id"
        );
        query.setParameter("match_id", matchId.getValue());
        query.setParameter("tournament_id", tournament.id().getValue());
        query.executeUpdate();

        entityManager.persist(matchFinished);
        return matchFinished;
    }
}
