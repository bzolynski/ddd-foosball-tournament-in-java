package foosball.infrastructure.persistence.hibernate;

import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.id.IdentifierGenerator;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class IdentityGenerator implements IdentifierGenerator {

    @Override
    public Serializable generate(SessionImplementor session, Object object)
    {
        Random random = new Random();
        SimpleDateFormat sdf = new SimpleDateFormat("HHmmss");
        String id = String.valueOf(random.nextInt(899) + 100) + sdf.format(new Date());
        return Integer.parseInt(id);
    }
}
