package foosball.infrastructure.persistence.hibernate;

import foosball.domain.exception.InvalidGoalKeeperNameException;
import foosball.domain.exception.InvalidStrikerNameException;
import foosball.domain.exception.InvalidTeamNameException;
import foosball.domain.exception.TeamNotFoundException;
import foosball.domain.model.Team;
import foosball.domain.repository.TeamRepositoryInterface;
import foosball.domain.model.TeamFactory;
import foosball.domain.valueobject.TeamId;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
@Transactional
public class TeamRepository implements TeamRepositoryInterface {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Team create(String name, String strikerName, String goalKeeperName)
            throws InvalidStrikerNameException, InvalidGoalKeeperNameException, InvalidTeamNameException {
        TeamFactory teamFactory = new TeamFactory();
        Team team = teamFactory.create(name, strikerName, goalKeeperName);
        entityManager.persist(team);
        return team;
    }

    @Override
    public Team getById(TeamId teamId) throws TeamNotFoundException {
        Team team = entityManager.find(Team.class, teamId.getValue());
        if (team == null) {
            throw new TeamNotFoundException("Team with id: " + String.valueOf(teamId.getValue()) + " not found");
        }
        return team;
    }
}
