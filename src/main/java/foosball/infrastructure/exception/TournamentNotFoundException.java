package foosball.infrastructure.exception;

public class TournamentNotFoundException extends Exception {
    public TournamentNotFoundException(String message) {
        super(message);
    }
}
