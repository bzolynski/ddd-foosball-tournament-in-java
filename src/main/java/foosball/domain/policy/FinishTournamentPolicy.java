package foosball.domain.policy;

import foosball.domain.model.MatchFinished;
import foosball.domain.model.MatchInterface;

import java.util.List;

public class FinishTournamentPolicy {
    public static Boolean isAllowed(List<MatchInterface> matches) {
        for (MatchInterface match: matches) {
            if (!(match instanceof MatchFinished)) {
                return false;
            }
        }
        return true;
    }
}
