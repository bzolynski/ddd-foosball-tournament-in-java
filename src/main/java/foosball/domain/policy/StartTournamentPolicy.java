package foosball.domain.policy;

import foosball.domain.model.Tournament;

public class StartTournamentPolicy {
    public static Boolean isAllowed(Tournament tournament) {
        return tournament.teams().size() >= 2;
    }
}
