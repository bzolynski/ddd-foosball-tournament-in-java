package foosball.domain.model;

import foosball.domain.exception.InvalidMatchArgumentException;
import foosball.domain.valueobject.MatchId;
import foosball.domain.valueobject.TeamId;
import foosball.domain.valueobject.TournamentId;

public class Match extends AbstractMatch implements MatchInterface {

    /**
     * Hibernate requires default constructor
     */
    Match() {

    }

    public Match(MatchId matchId, TournamentId tournamentId, TeamId firstTeamId, TeamId secondTeamId)
    throws InvalidMatchArgumentException {
        if (firstTeamId.equals(secondTeamId)) {
            throw new InvalidMatchArgumentException(
                    "Team with id: " + firstTeamId.getValue() + " cannot play against itself"
            );
        }
        setId(matchId.getValue());
        setTournamentId(tournamentId.getValue());
        setFirstTeamId(firstTeamId);
        setSecondTeamId(secondTeamId);
    }

    MatchFinished finish(int firstTeamScore, int secondTeamScore) {
        return new MatchFinished(this, firstTeamScore, secondTeamScore);
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof MatchInterface)) {
            return false;
        }
        MatchInterface match = (Match)object;

        Boolean orderMatching = match.getFirstTeamId().equals(getFirstTeamId())
                && match.getSecondTeamId().equals(getSecondTeamId());

        Boolean swappedMatching = match.getFirstTeamId().equals(getSecondTeamId())
                && match.getSecondTeamId().equals(getFirstTeamId());

        return match.tournamentId().equals(tournamentId()) && (orderMatching || swappedMatching);
    }

    @Override
    public int hashCode() {
        return id;
    }


//    public void setFirstTeamId(TeamId teamId) {
//        this.firstTeamId = teamId;
//    }
//
//    public void setSecondTeamId(TeamId teamId) {
//        this.secondTeamId = teamId;
//    }

    public TournamentId tournamentId() {
        return new TournamentId(tournamentId);
    }

    /**
     * Required by Hibernate, and needed to be public because of the MatchInterface
     */
    public void setFirstTeamScore(int score) {
        this.firstTeamScore = score;
    }

    /**
     * Required by Hibernate, and needed to be public because of the MatchInterface
     */
    public void setSecondTeamScore(int score) {
        this.secondTeamScore = score;
    }
}
