package foosball.domain.model;

import foosball.domain.exception.InvalidPlayerNameException;

abstract class Player {
    private String name;

    Player(String name) throws InvalidPlayerNameException {
        name = name.trim();
        if (name.isEmpty()) {
            throw new InvalidPlayerNameException("player name cannot be empty");
        }
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
