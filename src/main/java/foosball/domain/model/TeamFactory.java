package foosball.domain.model;

import foosball.domain.exception.InvalidGoalKeeperNameException;
import foosball.domain.exception.InvalidStrikerNameException;
import foosball.domain.exception.InvalidTeamNameException;
import foosball.domain.valueobject.TeamId;

public class TeamFactory {
    public Team create(TeamId teamId, String name, String strikerName, String goalKeeperName)
            throws InvalidStrikerNameException, InvalidGoalKeeperNameException, InvalidTeamNameException {
        Striker striker = new Striker(strikerName);
        GoalKeeper goalKeeper = new GoalKeeper(goalKeeperName);
        return new Team(teamId, name, striker, goalKeeper);
    }

    public Team create(String name, String strikerName, String goalKeeperName)
            throws InvalidStrikerNameException, InvalidGoalKeeperNameException, InvalidTeamNameException {
        Striker striker = new Striker(strikerName);
        GoalKeeper goalKeeper = new GoalKeeper(goalKeeperName);
        return new Team(name, striker, goalKeeper);
    }
}
