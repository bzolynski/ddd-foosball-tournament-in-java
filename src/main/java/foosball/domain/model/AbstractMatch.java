package foosball.domain.model;

import foosball.domain.shared.Entity;
import foosball.domain.valueobject.MatchId;
import foosball.domain.valueobject.TeamId;
import foosball.domain.valueobject.TournamentId;

import java.io.Serializable;

public class AbstractMatch extends Entity implements Serializable {

    TeamId firstTeamId;

    TeamId secondTeamId;

    int tournamentId;

    Integer firstTeamScore = 0;

    Integer secondTeamScore = 0;

    public TournamentId tournamentId() {
        return new TournamentId(tournamentId);
    }

    protected void setTournamentId(int id) {
        tournamentId = id;
    }

    public MatchId id() {
        return new MatchId(id);
    }

    public int getTournamentId() {
        return tournamentId;
    }

    public TeamId getFirstTeamId() {
        return firstTeamId;
    }

    public TeamId getSecondTeamId() {
        return secondTeamId;
    }

    public Integer getFirstTeamScore() {
        return firstTeamScore;
    }

    public Integer getSecondTeamScore() {
        return secondTeamScore;
    }

    public void setFirstTeamId(TeamId teamId) {
        this.firstTeamId = teamId;
    }

    public void setSecondTeamId(TeamId teamId) {
        this.secondTeamId = teamId;
    }
}
