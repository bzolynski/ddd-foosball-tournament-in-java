package foosball.domain.model;

import foosball.domain.valueobject.TeamId;

public class MatchFinished extends AbstractMatch implements MatchInterface {

    /**
     * Hibernate requires default constructor
     */
    MatchFinished() {

    }

    MatchFinished(Match match, int firstTeamScore, int secondTeamScore) {
        setId(match.id().getValue());
        setFirstTeamId(match.getFirstTeamId());
        setSecondTeamId(match.getSecondTeamId());
        setTournamentId(match.tournamentId().getValue());
        setFirstTeamScore(firstTeamScore);
        setSecondTeamScore(secondTeamScore);
    }

    public void setFirstTeamId(TeamId teamId) {
        super.firstTeamId = teamId;
    }

    public void setSecondTeamId(TeamId teamId) {
        super.secondTeamId = teamId;
    }

    private void setFirstTeamScore(int score) {
        this.firstTeamScore = score;
    }

    private void setSecondTeamScore(int score) {
        this.secondTeamScore = score;
    }
}
