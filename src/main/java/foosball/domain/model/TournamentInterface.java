package foosball.domain.model;

import foosball.domain.valueobject.AggregateRootId;

public interface TournamentInterface {
    AggregateRootId id();
}
