package foosball.domain.model;

import foosball.domain.exception.InvalidStrikerNameException;

public class Striker {
    private String name;

    Striker(String name) throws InvalidStrikerNameException {
        name = name.trim();
        if (name.isEmpty()) {
            throw new InvalidStrikerNameException("Striker name cannot be empty");
        }
        setName(name);
    }

    /**
     * Hibernate requires default constructor
     */
    private Striker() {
    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        this.name = name;
    }
}
