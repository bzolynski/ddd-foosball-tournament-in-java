package foosball.domain.model;

import foosball.domain.shared.AggregateRoot;
import foosball.domain.exception.InvalidTeamNameException;
import foosball.domain.valueobject.TeamId;

public class Team extends AggregateRoot {
    private String name;
    private Striker striker;
    private GoalKeeper goalKeeper;

    /**
     * Hibernate requires default constructor
     */
    private Team() {

    }

    public Team(TeamId teamId, String name, Striker striker, GoalKeeper goalKeeper) throws InvalidTeamNameException {
        setId(teamId.getValue());
        setName(name);
        setStriker(striker);
        setGoalKeeper(goalKeeper);
    }

    public Team(String name, Striker striker, GoalKeeper goalKeeper) throws InvalidTeamNameException {
        name = name.trim();
        if (name.isEmpty()) {
            throw new InvalidTeamNameException("Team name cannot be empty");
        }
        setName(name);
        setStriker(striker);
        setGoalKeeper(goalKeeper);
    }

    private void setName(String name) throws InvalidTeamNameException {
        name = name.trim();
        if (name.isEmpty()) {
            throw new InvalidTeamNameException("Team name cannot be empty");
        }
        this.name = name;
    }

    private void setStriker(Striker striker) {
        this.striker = striker;
    }

    private void setGoalKeeper(GoalKeeper goalKeeper) {
        this.goalKeeper = goalKeeper;
    }

    public TeamId id() {
        return new TeamId(getId());
    }

    public int getId() { return id; }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Team)) {
            return false;
        }
        Team team = (Team)object;
        return name.equals(team.getName()) && id().equals(team.id());
    }

    public String getName() {
        return name;
    }

    public Striker getStriker() {
        return striker;
    }

    public GoalKeeper getGoalKeeper() {
        return goalKeeper;
    }
}
