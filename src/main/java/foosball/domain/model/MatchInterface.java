package foosball.domain.model;

import foosball.domain.valueobject.MatchId;
import foosball.domain.valueobject.TeamId;
import foosball.domain.valueobject.TournamentId;

public interface MatchInterface {
    MatchId id();
    int getTournamentId();
    TournamentId tournamentId();
    TeamId getFirstTeamId();
    TeamId getSecondTeamId();
    Integer getFirstTeamScore();
    Integer getSecondTeamScore();
    void setFirstTeamId(TeamId teamId);
    void setSecondTeamId(TeamId teamId);
}
