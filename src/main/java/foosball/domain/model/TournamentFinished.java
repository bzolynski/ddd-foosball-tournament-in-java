package foosball.domain.model;

import foosball.domain.valueobject.TeamId;
import java.util.HashMap;
import java.util.Map;

public class TournamentFinished extends Tournament implements TournamentInterface {

    private HashMap<Integer, Integer> teamPoints = new HashMap<>();
    private HashMap<Integer, Integer> teamGoalsFor = new HashMap<>();
    private HashMap<Integer, Integer> teamGoalsAgainst = new HashMap<>();

    /**
     * Hibernate requires default constructor
     */
    TournamentFinished() {

    }

    TournamentFinished(TournamentInProgress tournament) {
        setId(tournament.id().getValue());
        teams = tournament.teams();
        matches = tournament.matches();
    }

    public Integer getWinnerTeamId() {
        for (TeamId teamId: teams) {
            teamPoints.put(teamId.getValue(), 0);
            teamGoalsFor.put(teamId.getValue(), 0);
            teamGoalsAgainst.put(teamId.getValue(), 0);
        }
        prepare();
        return findWinnerTeamId();
    }

    private void prepare() {
        for (MatchInterface match: matches) {
            if (match.getFirstTeamScore() > match.getSecondTeamScore()) {
                addPointsToTeam(match.getFirstTeamId(), 3);
            } else if (match.getFirstTeamScore() < match.getSecondTeamScore()) {
                addPointsToTeam(match.getSecondTeamId(), 3);
            } else {
                addPointsToTeam(match.getFirstTeamId(), 1);
                addPointsToTeam(match.getSecondTeamId(), 1);
            }
            addGoalsFor(match.getFirstTeamId(), match.getFirstTeamScore());
            addGoalsFor(match.getSecondTeamId(), match.getSecondTeamScore());
            addGoalsAgainst(match.getFirstTeamId(), match.getSecondTeamScore());
            addGoalsAgainst(match.getSecondTeamId(), match.getFirstTeamScore());
        }
    }

    private int findWinnerTeamId() {
        int bestTeamId = 0;
        int highestScore = 0;
        for (Map.Entry<Integer, Integer> entry: teamPoints.entrySet()) {
            int teamId = entry.getKey();
            int teamScore = entry.getValue();
            if (teamScore > highestScore) {
                highestScore = teamScore;
                bestTeamId = teamId;
            } else if (bestTeamId > 0 && teamScore == highestScore) {
                if (teamGoalsFor.get(teamId) > teamGoalsFor.get(bestTeamId)) {
                    bestTeamId = teamId;
                } else if (teamGoalsFor.get(teamId).equals(teamGoalsFor.get(bestTeamId))) {
                    if (teamGoalsAgainst.get(teamId) < teamGoalsAgainst.get(bestTeamId)) {
                        bestTeamId = teamId;
                    }
                }
            }
        }
        return bestTeamId;
    }

    private void addPointsToTeam(TeamId teamId, int points) {
        teamPoints.put(teamId.getValue(), teamPoints.get(teamId.getValue()) + points);
    }

    private void addGoalsFor(TeamId teamId, int goals) {
        teamGoalsFor.put(teamId.getValue(), teamGoalsFor.get(teamId.getValue()) + goals);
    }

    private void addGoalsAgainst(TeamId teamId, int goals) {
        teamGoalsAgainst.put(teamId.getValue(), teamGoalsAgainst.get(teamId.getValue()) + goals);
    }
}
