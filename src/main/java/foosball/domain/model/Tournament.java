package foosball.domain.model;

import foosball.domain.shared.AggregateRoot;
import foosball.domain.valueobject.TeamId;
import foosball.domain.valueobject.TournamentId;

import java.util.*;

abstract public class Tournament extends AggregateRoot implements TournamentInterface {
    protected List<MatchInterface> matches;

    protected List<TeamId> teams = new ArrayList<>();

    protected void setTeams(List<TeamId> teams) {
        this.teams = teams;
    }

    protected void setMatches(List<MatchInterface> matches) {
        this.matches = matches;
    }

    /**
     * Public method allows a property in Hibernate to be visible in API response
     */
    public List<TeamId> getTeams() {
        return teams;
    }

    /**
     * Public method allows a property in Hibernate to be visible in API response
     */
    public List<MatchInterface> getMatches() {
        return matches;
    }

    @Override
    public TournamentId id() {
        return new TournamentId(getId());
    }

    protected void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public List<TeamId> teams() {
        return getTeams();
    }

    public List<MatchInterface> matches() {
        return matches;
    }
}
