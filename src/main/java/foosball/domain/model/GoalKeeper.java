package foosball.domain.model;

import foosball.domain.exception.InvalidGoalKeeperNameException;

public class GoalKeeper {
    private String name;

    GoalKeeper(String name) throws InvalidGoalKeeperNameException {
        name = name.trim();
        if (name.isEmpty()) {
            throw new InvalidGoalKeeperNameException("Goal keeper name cannot be empty");
        }
        setName(name);
    }

    /**
     * Hibernate requires default constructor
     */
    private GoalKeeper() {
    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        this.name = name;
    }
}
