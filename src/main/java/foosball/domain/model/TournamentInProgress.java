package foosball.domain.model;

import foosball.domain.exception.InvalidMatchArgumentException;
import foosball.domain.exception.MatchNotFoundException;
import foosball.domain.exception.TournamentStartException;
import foosball.domain.policy.StartTournamentPolicy;
import foosball.domain.service.MatchesInTournamentService;
import foosball.domain.valueobject.MatchId;
import foosball.domain.exception.TournamentFinishException;
import foosball.domain.policy.FinishTournamentPolicy;

import java.util.List;

public class TournamentInProgress extends Tournament implements TournamentInterface {

    /**
     * Hibernate requires default constructor
     */
    TournamentInProgress() {

    }

    TournamentInProgress(TournamentInit tournament) throws InvalidMatchArgumentException {
        setId(tournament.id().getValue());
        setTeams(tournament.teams());
        setMatches();
    }

    private void setMatches() throws InvalidMatchArgumentException {
        MatchesInTournamentService matchesInTournamentService = new MatchesInTournamentService(
                teams(),
                id()
        );
        List<MatchInterface> matches = matchesInTournamentService.getMatches();
        setMatches(matches);
    }

    public MatchFinished setMatchResult(MatchId matchId, Integer firstTeamScore, Integer secondTeamScore)
        throws MatchNotFoundException{
        Match match = getNotFinishedMatch(matchId);
        MatchFinished matchFinished = match.finish(firstTeamScore, secondTeamScore);
        setMatchResult(matchFinished);
        return matchFinished;
    }

    public TournamentFinished finish() throws TournamentFinishException {
        if (!FinishTournamentPolicy.isAllowed(matches)) {
            throw new TournamentFinishException(
                    "Some matchesCollection weren't played yet, unable to finish the tournament"
            );
        }
        return new TournamentFinished(this);
    }

    private Match getNotFinishedMatch(MatchId matchId) throws MatchNotFoundException {
        for (MatchInterface match: matches) {
            if (match.id().equals(matchId) && match instanceof Match) {
                return (Match)match;
            }
        }

        throw new MatchNotFoundException(
                new StringBuilder() .
                        append("Match with id: ") .
                        append(matchId.getValue()) .
                        append(" not found in collection for tournament id: ") .
                        append(id().getValue()) .
                        toString()
        );
    }

    private void setMatchResult(MatchFinished matchFinished) {
        for (int key = 0; key < matches.size(); key++) {
            MatchInterface match = matches.get(key);
            if (matchFinished.id().equals(match.id())) {
                matches.set(key, matchFinished);
            }
        }
    }
}
