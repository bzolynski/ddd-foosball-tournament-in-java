package foosball.domain.model;

import foosball.domain.exception.InvalidMatchArgumentException;
import foosball.domain.service.MatchesInTournamentService;
import foosball.domain.exception.DuplicatedTeamException;
import foosball.domain.valueobject.TeamId;
import foosball.domain.exception.TournamentStartException;
import foosball.domain.policy.StartTournamentPolicy;
import foosball.domain.valueobject.TournamentId;

import java.util.List;

public class TournamentInit extends Tournament implements TournamentInterface {

    /**
     * Hibernate requires default constructor
     */
    public TournamentInit() {

    }

    public TournamentInit(TournamentId tournamentId) {
        setId(tournamentId.getValue());
    }

    public void addTeam(Team team) throws DuplicatedTeamException {
        validateTeam(team);
        teams.add(new TeamId(team.getId()));
    }

    public TournamentInProgress start() throws TournamentStartException, InvalidMatchArgumentException {
        if (!StartTournamentPolicy.isAllowed(this)) {
            throw new TournamentStartException("Insufficient amount of teams");
        }
        return new TournamentInProgress(this);
    }

    private void validateTeam(Team team) throws DuplicatedTeamException {
        for (TeamId id: teams) {
            if (id.equals(team.id())) {
                throw new DuplicatedTeamException("Team already added to tournament");
            }
        }
    }
}
