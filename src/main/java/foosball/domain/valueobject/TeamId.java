package foosball.domain.valueobject;

public class TeamId extends AggregateRootId {

    /**
     * Hibernate requires default constructor
     */
    private TeamId() {

    }

    public TeamId(int value) {
        super(value);
    }
}
