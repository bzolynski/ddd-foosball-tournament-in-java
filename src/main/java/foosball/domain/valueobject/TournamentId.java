package foosball.domain.valueobject;

public class TournamentId extends AggregateRootId {
    public TournamentId(int value) {
        super(value);
    }
}
