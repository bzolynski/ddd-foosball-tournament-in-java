package foosball.domain.valueobject;

public class EntityId {
    private Integer id;

    public EntityId(Integer id) {
        this.id = id;
    }

    public int getValue() {
        return this.id;
    }

    @Override
    public boolean equals(Object object) {
        return object instanceof EntityId && id == ((EntityId) object).getValue();
    }

    @Override
    public int hashCode() {
        return id;
    }
}
