package foosball.domain.valueobject;

public class AggregateRootId {
    private int value;

    /**
     * Hibernate requires default constructor
     */
    AggregateRootId() {

    }

    public AggregateRootId(int value) {
        setValue(value);
    }

    private void setValue(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }

    @Override
    public boolean equals(Object object) {
        return object instanceof AggregateRootId && value == ((AggregateRootId) object).getValue();
    }

    @Override
    public int hashCode() {
        return value;
    }
}
