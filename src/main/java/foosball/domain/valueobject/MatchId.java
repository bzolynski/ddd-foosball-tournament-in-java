package foosball.domain.valueobject;

public class MatchId extends EntityId {
    public MatchId(int value) {
        super(value);
    }
}
