package foosball.domain.service;

import foosball.domain.exception.InvalidMatchArgumentException;
import foosball.domain.model.Match;
import foosball.domain.model.MatchInterface;
import foosball.domain.valueobject.MatchId;
import foosball.domain.valueobject.TeamId;
import foosball.domain.valueobject.TournamentId;

import java.util.ArrayList;
import java.util.List;

public class MatchesInTournamentService {

    private List<TeamId> teamsIdCollection;
    private TournamentId tournamentId;
    private List<MatchInterface> matches;

    public MatchesInTournamentService(List<TeamId> teamsIdsCollection, TournamentId tournamentId)
            throws InvalidMatchArgumentException {
        this.teamsIdCollection = teamsIdsCollection;
        this.tournamentId = tournamentId;
        createMatches();
    }

    public List<MatchInterface> getMatches() {
        return matches;
    }

    private void createMatches() throws InvalidMatchArgumentException {
        int matchId = 0;
        matches = new ArrayList<>();
        for (TeamId firstTeamId: teamsIdCollection) {
            for (TeamId secondTeamId: teamsIdCollection) {
                if (!firstTeamId.equals(secondTeamId)) {
                    Match match = new Match(
                            new MatchId(matchId + 1),
                            tournamentId,
                            firstTeamId,
                            secondTeamId
                    );
                    if (!isMatchInPool(match)) {
                        matches.add(match);
                        matchId += 1;
                    }
                }
            }
        }
    }

    private Boolean isMatchInPool(Match match) {
        if (matches.isEmpty()) {
            return false;
        }
        for (MatchInterface aMatch: matches) {
            if (match.equals(aMatch)) {
                return true;
            }
        }
        return false;
    }
}
