package foosball.domain.shared;

import foosball.domain.valueobject.AggregateRootId;

public abstract class AggregateRoot {

    protected int id;

    protected void setId(int id) {
        this.id = id;
    }

    /**
     * Method used by Hibernate only
     */
    private int getId() {
        return id;
    }

    public AggregateRootId id() {
        return new AggregateRootId(id);
    }
}
