package foosball.domain.shared;

import foosball.domain.valueobject.EntityId;

public abstract class Entity {
    protected int id;

    public EntityId id() {
        return new EntityId(id);
    }

    public int getId() {
        return id;
    }

    protected void setId(int id) {
        this.id = id;
    }
}
