package foosball.domain.shared;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

// This generator is used by in-memory repositories to set id for Aggregate Roots
public class IdentityGenerator {
    public static int get() {
        Random random = new Random();
        SimpleDateFormat sdf = new SimpleDateFormat("HHmmss");
        String id = String.valueOf(random.nextInt(899) + 100) + sdf.format(new Date());
        return Integer.parseInt(id);
    }
}
