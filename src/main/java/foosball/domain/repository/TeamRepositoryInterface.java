package foosball.domain.repository;

import foosball.domain.exception.*;
import foosball.domain.model.*;
import foosball.domain.valueobject.TeamId;

public interface TeamRepositoryInterface {
    Team create(String name, String strikerName, String goalKeeperName)
            throws InvalidStrikerNameException, InvalidGoalKeeperNameException, InvalidTeamNameException;

    Team getById(TeamId teamId) throws TeamNotFoundException;
}
