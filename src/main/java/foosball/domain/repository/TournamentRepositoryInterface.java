package foosball.domain.repository;

import foosball.domain.exception.*;
import foosball.domain.model.*;
import foosball.domain.valueobject.MatchId;
import foosball.domain.valueobject.TournamentId;
import foosball.infrastructure.exception.TournamentNotFoundException;

import java.util.ArrayList;

public interface TournamentRepositoryInterface {
    TournamentInit create();

    TournamentInProgress start(TournamentInit tournamentInit)
            throws TournamentNotFoundException, TournamentStartException, InvalidMatchArgumentException;

    TournamentInit addTeam(TournamentId tournamentId, Team team) throws TournamentNotFoundException, DuplicatedTeamException;

    TournamentInit getTournamentInInitStateById(TournamentId tournamentId) throws TournamentNotFoundException;

    TournamentInProgress getTournamentInProgressStateById(TournamentId tournamentId) throws TournamentNotFoundException;

    TournamentFinished getTournamentInFinishedStateById(TournamentId tournamentId) throws TournamentNotFoundException;

    ArrayList<TournamentInit> findTournamentsInInitState();

    ArrayList<TournamentInProgress> findTournamentsInProgressState();

    ArrayList<TournamentFinished> findTournamentsInFinishedState();

    TournamentFinished finish(TournamentInProgress tournamentInProgress) throws TournamentFinishException;

    MatchFinished finishMatch(TournamentInProgress tournament,
                              MatchId matchId,
                              int firstTeamScore,
                              int secondTeamScore
    ) throws MatchNotFoundException, TournamentNotFoundException;
}
