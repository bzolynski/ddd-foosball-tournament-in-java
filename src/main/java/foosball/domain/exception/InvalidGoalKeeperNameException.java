package foosball.domain.exception;

public class InvalidGoalKeeperNameException extends Exception {
    public InvalidGoalKeeperNameException(String message) {
        super(message);
    }
}
