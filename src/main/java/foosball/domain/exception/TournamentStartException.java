package foosball.domain.exception;

public class TournamentStartException extends Exception {
    public TournamentStartException(String message) {
        super(message);
    }
}
