package foosball.domain.exception;

public class InvalidPlayerNameException extends Exception {
    public InvalidPlayerNameException(String message) {
        super(message);
    }
}
