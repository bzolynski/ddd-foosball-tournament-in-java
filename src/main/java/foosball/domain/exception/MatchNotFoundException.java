package foosball.domain.exception;

public class MatchNotFoundException extends Exception {
    public MatchNotFoundException(String message) {
        super(message);
    }
}
