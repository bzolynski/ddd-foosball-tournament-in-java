package foosball.domain.exception;

public class InvalidStrikerNameException extends Exception {
    public InvalidStrikerNameException(String message) {
        super(message);
    }
}
