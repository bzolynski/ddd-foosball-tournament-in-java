package foosball.domain.exception;

public class TournamentFinishException extends Exception {
    public TournamentFinishException(String message) {
        super(message);
    }
}
