package foosball.domain.exception;

public class DuplicatedTeamException extends Exception {
    public DuplicatedTeamException(String message) {
        super(message);
    }
}
