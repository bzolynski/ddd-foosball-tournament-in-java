package foosball.domain.exception;

public class InvalidMatchArgumentException extends Exception {
    public InvalidMatchArgumentException(String message) {
        super(message);
    }
}
