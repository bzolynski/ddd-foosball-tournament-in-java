package foosball.domain.exception;

public class InvalidTeamNameException extends Exception {
    public InvalidTeamNameException(String message) {
        super(message);
    }
}
