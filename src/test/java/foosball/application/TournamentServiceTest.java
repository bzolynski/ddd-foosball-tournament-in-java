package foosball.application;

import foosball.domain.model.MatchInterface;
import foosball.domain.exception.TeamNotFoundException;
import foosball.domain.model.Team;
import foosball.domain.repository.TeamRepositoryInterface;
import foosball.domain.model.TournamentFinished;
import foosball.domain.model.TournamentInit;
import foosball.domain.repository.TournamentRepositoryInterface;
import foosball.infrastructure.exception.TournamentNotFoundException;
import foosball.infrastructure.persistence.inmemory.InMemoryTeamRepository;
import foosball.infrastructure.persistence.inmemory.InMemoryTournamentRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class TournamentServiceTest {

    private TournamentService tournamentService;

    @Before
    public void setUp() {
         TournamentRepositoryInterface tournamentRepository = new InMemoryTournamentRepository();
         TeamRepositoryInterface teamRepository = new InMemoryTeamRepository();
         tournamentService = new TournamentService(
                 tournamentRepository,
                 teamRepository
         );
    }

    @Test(expected = TournamentNotFoundException.class)
    public void testTournamentInInitStateDoesNotExistOnEmptyList() throws Throwable {
        tournamentService.getTournamentInInitStateById(1);
    }

    @Test(expected = TournamentNotFoundException.class)
    public void testTournamentInProgressStateDoesNotExistOnEmptyList() throws Throwable {
        tournamentService.getTournamentInProgressStateById(1);
    }

    @Test(expected = TournamentNotFoundException.class)
    public void testTournamentInFinishedStateDoesNotExistOnEmptyList() throws Throwable {
        tournamentService.getTournamentInFinishedStateById(1);
    }

    @Test(expected = TournamentNotFoundException.class)
    public void testTournamentNotInProgressState() throws Throwable {
        TournamentInit tournament = tournamentService.createTournament();
        tournamentService.getTournamentInProgressStateById(tournament.id().getValue());
    }

    @Test(expected = TournamentNotFoundException.class)
    public void testTournamentNotInFinishedState() throws Throwable {
        TournamentInit tournament = tournamentService.createTournament();
        tournamentService.getTournamentInFinishedStateById(tournament.id().getValue());
    }

    @Test(expected = TournamentNotFoundException.class)
    public void testTournamentInInitStateDoesNotExist() throws Throwable {
        tournamentService.createTournament();
        tournamentService.getTournamentInInitStateById(12345);
    }

    @Test(expected = TournamentNotFoundException.class)
    public void testTournamentInProgressStateDoesNotExist() throws Throwable {
        tournamentService.createTournament();
        tournamentService.getTournamentInProgressStateById(12345);
    }

    @Test(expected = TournamentNotFoundException.class)
    public void testTournamentInFinishedStateDoesNotExist() throws Throwable {
        tournamentService.createTournament();
        tournamentService.getTournamentInFinishedStateById(12345);
    }

    @Test(expected = TournamentNotFoundException.class)
    public void testTournamentInInitStateDoesNotExistAnymore() throws Throwable {
        TournamentInit tournament = tournamentService.createTournament();
        int tournamentId = tournament.id().getValue();

        Team team1 = tournamentService.createTeam(
                "Team #1", "Pele", "Edwin van der Sar"
        );
        Team team2 = tournamentService.createTeam(
                "Team #2", "Alessandro Del Piero", "Gianluigi Buffon"
        );
        Team team3 = tournamentService.createTeam(
                "Team #3",
                "Diego Maradona",
                "David Seaman"
        );
        Team team4 = tournamentService.createTeam(
                "Team #4",
                "Zinedine Zidane",
                "Peter Schmeichel"
        );
        tournamentService.addTeamToTournament(tournamentId, team1.id().getValue());
        tournamentService.addTeamToTournament(tournamentId, team2.id().getValue());
        tournamentService.addTeamToTournament(tournamentId, team3.id().getValue());
        tournamentService.addTeamToTournament(tournamentId, team4.id().getValue());

        Assert.assertTrue(tournamentService.findTournamentsInInitState().size() == 1);
        Assert.assertTrue(tournamentService.findTournamentsInProgressState().size() == 0);
        Assert.assertTrue(tournamentService.findTournamentsInFinishedState().size() == 0);

        tournamentService.startTournament(tournamentId);
        tournamentService.getTournamentInInitStateById(tournament.id().getValue());
    }

    @Test
    public void testGetTeam() throws Throwable {
        Team team = tournamentService.createTeam(
                "Team #1", "Pele", "Edwin van der Sar"
        );
        Team teamResult = tournamentService.getTeamById(team.getId());
        Assert.assertTrue(teamResult.equals(team));
    }

    @Test
    public void testPlayTournament() throws Throwable {

        Assert.assertTrue(tournamentService.findTournamentsInInitState().size() == 0);
        Assert.assertTrue(tournamentService.findTournamentsInProgressState().size() == 0);
        Assert.assertTrue(tournamentService.findTournamentsInFinishedState().size() == 0);

        TournamentInit tournament = tournamentService.createTournament();
        int tournamentId = tournament.id().getValue();

        Team team1 = tournamentService.createTeam(
                "Team #1", "Pele", "Edwin van der Sar"
        );
        Team team2 = tournamentService.createTeam(
                "Team #2", "Alessandro Del Piero", "Gianluigi Buffon"
        );
        Team team3 = tournamentService.createTeam(
                "Team #3",
                "Diego Maradona",
                "David Seaman"
        );
        Team team4 = tournamentService.createTeam(
                "Team #4",
                "Zinedine Zidane",
                "Peter Schmeichel"
        );
        tournamentService.addTeamToTournament(tournamentId, team1.id().getValue());
        tournamentService.addTeamToTournament(tournamentId, team2.id().getValue());
        tournamentService.addTeamToTournament(tournamentId, team3.id().getValue());
        tournamentService.addTeamToTournament(tournamentId, team4.id().getValue());

        Assert.assertTrue(tournamentService.findTournamentsInInitState().size() == 1);
        Assert.assertTrue(tournamentService.findTournamentsInProgressState().size() == 0);
        Assert.assertTrue(tournamentService.findTournamentsInFinishedState().size() == 0);

        tournamentService.startTournament(tournamentId);

        Assert.assertTrue(tournamentService.findTournamentsInInitState().size() == 0);
        Assert.assertTrue(tournamentService.findTournamentsInProgressState().size() == 1);
        Assert.assertTrue(tournamentService.findTournamentsInFinishedState().size() == 0);

        tournamentService.getTournamentInProgressStateById(tournamentId);

        List<MatchInterface> matches = tournamentService.getMatchesForTournamentInProgress(tournamentId);
        Assert.assertSame(6, matches.size());

        ArrayList<Integer> firstTeamResultTable = new ArrayList<>();
        firstTeamResultTable.add(2);
        firstTeamResultTable.add(3);
        firstTeamResultTable.add(3);
        firstTeamResultTable.add(2);
        firstTeamResultTable.add(0);
        firstTeamResultTable.add(0);

        ArrayList<Integer> secondTeamResultTable = new ArrayList<>();
        secondTeamResultTable.add(5);
        secondTeamResultTable.add(2);
        secondTeamResultTable.add(3);
        secondTeamResultTable.add(1);
        secondTeamResultTable.add(0);
        secondTeamResultTable.add(2);

        Integer matchKey = 0;
        for (MatchInterface match: matches) {
            tournamentService.finishMatch(
                    tournamentId,
                    match.id().getValue(),
                    firstTeamResultTable.get(matchKey),
                    secondTeamResultTable.get(matchKey)
            );
            matchKey += 1;
        }

        List<MatchInterface> finishedMatches = tournamentService.getMatchesForTournamentInProgress(tournamentId);
        Assert.assertSame(6, finishedMatches.size());

        matchKey = 0;
        for (MatchInterface match: finishedMatches) {
            Assert.assertSame(firstTeamResultTable.get(matchKey), match.getFirstTeamScore());
            Assert.assertSame(secondTeamResultTable.get(matchKey), match.getSecondTeamScore());
            matchKey += 1;
        }

        tournamentService.finishTournament(tournamentId);

        Assert.assertTrue(tournamentService.findTournamentsInInitState().size() == 0);
        Assert.assertTrue(tournamentService.findTournamentsInProgressState().size() == 0);
        Assert.assertTrue(tournamentService.findTournamentsInFinishedState().size() == 1);

        TournamentFinished tournamentFinished = tournamentService.getTournamentInFinishedStateById(tournamentId);

        Assert.assertTrue(team2.id().getValue() == tournamentFinished.getWinnerTeamId());
        Assert.assertTrue(
                team2.id().getValue() == tournamentService.getWinnerTeamForTournament(tournamentId)
        );
    }

    @Test(expected = TeamNotFoundException.class)
    public void testTeamNotFound() throws Throwable {
        TournamentInit tournament = tournamentService.createTournament();
        int tournamentId = tournament.id().getValue();
        tournamentService.createTeam("Team #1", "Pele", "Edwin van der Sar");
        tournamentService.addTeamToTournament(tournamentId, 100);
    }

    @Test(expected = TeamNotFoundException.class)
    public void testTeamNotFoundWithEmptyRepository() throws Throwable {
        TournamentInit tournament = tournamentService.createTournament();
        int tournamentId = tournament.id().getValue();
        tournamentService.addTeamToTournament(tournamentId, 100);
    }
}
