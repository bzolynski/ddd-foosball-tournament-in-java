package foosball.infrastructure;

import foosball.application.Application;
import foosball.infrastructure.persistence.hibernate.TeamRepository;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

// Probably these below are not needed...
//@Configuration
//@EnableTransactionManagement
//@EnableJpaRepositories
//@EnableAspectJAutoProxy(proxyTargetClass = true)
//@EnableConfigurationProperties



//@SpringBootApplication
//@SpringBootConfiguration



//@RunWith(SpringRunner.class)
//@Transactional
//@Configuration
//@EnableAutoConfiguration

@ComponentScan("foosball")

//@Resource(name="Team.hbm.xml")

@SpringBootTest(classes = { Application.class })

// @EnableConfigurationProperties
//@SpringBootConfiguration(Application.class)
//@BootstrapWith(SpringBootTestContextBootstrapper.class)
//@SpringBootApplication
@ImportResource("classpath:/resources/Team.hbm.xml")
@RunWith(SpringJUnit4ClassRunner.class)

//@Resource("Team.hbm.xml")
//@ContextConfiguration(loader=.class)

public class HibernateIntegrationTest {

    @Autowired
    private TeamRepository teamRepository;

    @Test
    @Ignore
    public void testCreateTeam() throws Throwable {
        teamRepository.create(
                "Team #1",
                "Pele",
                "Edwin van der Sar"
        );
    }
}
