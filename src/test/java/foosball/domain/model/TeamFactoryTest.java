package foosball.domain.model;

import foosball.domain.exception.InvalidTeamNameException;
import foosball.domain.valueobject.TeamId;
import org.junit.Assert;
import org.junit.Test;

public class TeamFactoryTest {
    @Test
    public void testInstantiation() throws Throwable {
        TeamFactory teamFactory = new TeamFactory();
        Team team = teamFactory.create(
                new TeamId(1),
                "Team #1",
                "Pele",
                "Edwin van der Sar"
        );
        Assert.assertTrue(team.getStriker() != null);
        Assert.assertTrue(team.getGoalKeeper() != null);
        Assert.assertTrue(team.getName().equals("Team #1"));
        Assert.assertTrue(team.getStriker().getName().equals("Pele"));
        Assert.assertTrue(team.getGoalKeeper().getName().equals("Edwin van der Sar"));
    }

    @Test(expected = InvalidTeamNameException.class)
    public void testInvalidNameTeamInstantiation() throws Throwable {
        TeamFactory teamFactory = new TeamFactory();
        teamFactory.create(new TeamId(1)," ", "Pele", "Edwin van der Sar");
    }
}
