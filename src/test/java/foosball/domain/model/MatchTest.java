package foosball.domain.model;

import foosball.domain.exception.InvalidMatchArgumentException;
import foosball.domain.valueobject.MatchId;
import foosball.domain.valueobject.TeamId;
import foosball.domain.valueobject.TournamentId;
import org.junit.Assert;
import org.junit.Test;

public class MatchTest {
    @Test
    public void testInstantiation() throws Throwable {
        TeamFactory teamFactory = new TeamFactory();
        Team team1 = teamFactory.create(
                new TeamId(1),
                "Team #1",
                "Pele",
                "Edwin van der Sar"
        );
        Team team2 = teamFactory.create(
                new TeamId(2),
                "Team #2",
                "Alessandro Del Piero",
                "Gianluigi Buffon"
        );
        Match match = new Match(
                new MatchId(1),
                new TournamentId(1),
                new TeamId(team1.id().getValue()),
                new TeamId(team2.id().getValue())
        );
        Assert.assertTrue(match.tournamentId().equals(new TournamentId(1)));

        MatchFinished matchFinished = match.finish(2, 3);
        Assert.assertSame(2, matchFinished.getFirstTeamScore());
        Assert.assertSame(3, matchFinished.getSecondTeamScore());
        Assert.assertSame(team1.id().getValue(), matchFinished.getFirstTeamId().getValue());
        Assert.assertSame(team2.id().getValue(), matchFinished.getSecondTeamId().getValue());
    }

    @Test
    public void testMatchEquals() throws Throwable {
        TeamFactory teamFactory = new TeamFactory();
        Team team1 = teamFactory.create(
                new TeamId(1),
                "Team #1",
                "Pele",
                "Edwin van der Sar"
        );
        Team team2 = teamFactory.create(
                new TeamId(2),
                "Team #2",
                "Alessandro Del Piero",
                "Gianluigi Buffon"
        );
        Team team3 = teamFactory.create(
                new TeamId(3),
                "Team #3",
                "Diego Maradona",
                "David Seaman"
        );
        Match match1 = new Match(
                new MatchId(1),
                new TournamentId(1),
                team1.id(),
                team2.id()
        );
        Match match2 = new Match(
                new MatchId(2),
                new TournamentId(1),
                team2.id(),
                team1.id()
        );
        Match match3 = new Match(
                new MatchId(2),
                new TournamentId(1),
                team2.id(),
                team3.id()
        );
        Match match4 = new Match(
                new MatchId(2),
                new TournamentId(2),
                team1.id(),
                team2.id()
        );
        Match match5 = new Match(
                new MatchId(2),
                new TournamentId(2),
                team2.id(),
                team1.id()
        );
        Assert.assertTrue(match1.equals(match1));
        Assert.assertTrue(match1.equals(match2));
        Assert.assertFalse(match1.equals(match3));
        Assert.assertFalse(match1.equals(match4));
        Assert.assertFalse(match1.equals(match5));
        Assert.assertFalse(match5.equals(new Object()));
        Assert.assertTrue(match1.hashCode() == match1.id().getValue());
    }

    @Test(expected = InvalidMatchArgumentException.class)
    public void testInvalidInstantiation() throws Throwable {
        TeamFactory teamFactory = new TeamFactory();
        Team team1 = teamFactory.create(
                new TeamId(1),
                "Team #1",
                "Pele",
                "Edwin van der Sar"
        );
        Team team2 = teamFactory.create(
                new TeamId(1),
                "Team #2",
                "Alessandro Del Piero",
                "Gianluigi Buffon"
        );
        new Match(
                new MatchId(1),
                new TournamentId(1),
                team1.id(),
                team2.id()
        );
    }
}
