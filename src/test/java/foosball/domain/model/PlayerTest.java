package foosball.domain.model;

import foosball.domain.exception.InvalidGoalKeeperNameException;
import foosball.domain.exception.InvalidStrikerNameException;
import org.junit.Assert;
import org.junit.Test;

public class PlayerTest {
    @Test
    public void testStrikerInstantiation() throws InvalidStrikerNameException {
        Striker striker = new Striker("my name");
        Assert.assertSame("my name", striker.getName());
    }

    @Test
    public void testGoalKeeperInstantiation() throws InvalidGoalKeeperNameException {
        GoalKeeper goalKeeper = new GoalKeeper("my name");
        Assert.assertSame("my name", goalKeeper.getName());
    }

    @Test(expected = InvalidStrikerNameException.class)
    public void testInvalidStrikerNameInstantiation() throws InvalidStrikerNameException {
        new Striker(" ");
    }

    @Test(expected = InvalidGoalKeeperNameException.class)
    public void testInvalidGoalKeeperNameInstantiation() throws InvalidGoalKeeperNameException {
        new GoalKeeper(" ");
    }
}
