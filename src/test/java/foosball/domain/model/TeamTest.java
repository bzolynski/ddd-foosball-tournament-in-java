package foosball.domain.model;

import foosball.domain.valueobject.TeamId;
import org.junit.Assert;
import org.junit.Test;

public class TeamTest {
    @Test
    public void teamEquals() throws Throwable {
        TeamFactory teamFactory = new TeamFactory();
        Team team1 = teamFactory.create(
                new TeamId(1),
                "Team #1",
                "Pele",
                "Edwin van der Sar"
        );
        Team team2 = teamFactory.create(
                new TeamId(2),
                "Team #2",
                "Alessandro Del Piero",
                "Gianluigi Buffon"
        );
        Team team3 = teamFactory.create(
                new TeamId(2),
                "Team #2",
                "Alessandro Del Piero",
                "Gianluigi Buffon"
        );

        Assert.assertFalse(team1.equals(team2));
        Assert.assertTrue(team2.equals(team3));
        Assert.assertFalse(team1.equals(new Object()));
    }
}
