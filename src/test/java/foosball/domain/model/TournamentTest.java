package foosball.domain.model;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import foosball.domain.exception.MatchNotFoundException;
import foosball.domain.valueobject.MatchId;
import foosball.domain.service.MatchesInTournamentService;
import foosball.domain.shared.IdentityGenerator;
import foosball.domain.exception.DuplicatedTeamException;
import foosball.domain.valueobject.TeamId;
import foosball.domain.exception.TournamentFinishException;
import foosball.domain.exception.TournamentStartException;
import foosball.domain.valueobject.TournamentId;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.*;

@RunWith(DataProviderRunner.class)
public class TournamentTest {
    @Test
    public void testInstantiation() throws Throwable {
        int id = IdentityGenerator.get();
        TournamentInit tournament = new TournamentInit(new TournamentId(id));
        for (Team team: getTeams()) {
            tournament.addTeam(team);
        }

        Assert.assertTrue(tournament.start() != null);
    }

    @Test(expected = DuplicatedTeamException.class)
    public void testInvalidTeamsInstantiation() throws Throwable {
        int id = IdentityGenerator.get();
        TournamentInit tournament = new TournamentInit(new TournamentId(id));
        for (Team team: getInvalidTeams()) {
            tournament.addTeam(team);
        }
    }

    @Test(expected = TournamentStartException.class)
    public void testFailWhenStartingTournamentWithOnlyOneTeam() throws Throwable {
        int id = IdentityGenerator.get();
        TournamentInit tournament = new TournamentInit(new TournamentId(id));
        ArrayList<Team> teams = getTeams();
        tournament.addTeam(teams.get(0));
        tournament.start();
    }

    @Test
    @UseDataProvider("resultScenarios")
    public void testWinningTeam(int scenarioIndex, int winnerTeamId) throws Throwable {
        int[][] results = getResults(scenarioIndex);

        TournamentId tournamentId = new TournamentId(1);
        TournamentInit tournament = new TournamentInit(tournamentId);
        List<TeamId> teamsIdCollection = new ArrayList<>();
        for (Team team: getTeams()) {
            tournament.addTeam(team);
            teamsIdCollection.add(new TeamId(team.getId()));
        }
        MatchesInTournamentService matchesService = new MatchesInTournamentService(teamsIdCollection, tournamentId);
        TournamentInProgress tournamentInProgress = tournament.start();
        List<MatchInterface> matches = matchesService.getMatches();

        int matchOrder = 0;
        for (MatchInterface match: matches) {
            tournamentInProgress.setMatchResult(
                    match.id(),
                    results[matchOrder][0],
                    results[matchOrder][1]
            );
            matchOrder += 1;
        }
        Assert.assertSame(4, tournamentInProgress.teams().size());
        Assert.assertSame(6, tournamentInProgress.matches().size());

        TournamentFinished tournamentFinished = tournamentInProgress.finish();
        Assert.assertSame(winnerTeamId, tournamentFinished.getWinnerTeamId());
    }

    @Test(expected = TournamentFinishException.class)
    public void testTournamentNotFinished() throws Throwable {
        TournamentId tournamentId = new TournamentId(1);
        TournamentInit tournament = new TournamentInit(tournamentId);
        List<TeamId> teamsIdCollection = new ArrayList<>();
        for (Team team: getTeams()) {
            tournament.addTeam(team);
            teamsIdCollection.add(new TeamId(team.getId()));
        }
        MatchesInTournamentService matchesService = new MatchesInTournamentService(teamsIdCollection, tournamentId);
        TournamentInProgress tournamentInProgress = tournament.start();
        List<MatchInterface> matches = matchesService.getMatches();

        MatchId matchId = matches.get(0).id();
        tournamentInProgress.setMatchResult(matchId, 1, 2);
        tournamentInProgress.finish();
    }

    @Test(expected = MatchNotFoundException.class)
    public void testNotFinishedMatchNotFoundInTournament() throws Throwable {
        TournamentId tournamentId = new TournamentId(1);
        TournamentInit tournament = new TournamentInit(tournamentId);
        List<TeamId> teamsIdCollection = new ArrayList<>();
        for (Team team: getTeams()) {
            tournament.addTeam(team);
            teamsIdCollection.add(new TeamId(team.getId()));
        }
        MatchesInTournamentService matchesService = new MatchesInTournamentService(teamsIdCollection, tournamentId);
        TournamentInProgress tournamentInProgress = tournament.start();
        List<MatchInterface> matches = matchesService.getMatches();

        MatchId matchId = matches.get(0).id();
        tournamentInProgress.setMatchResult(matchId, 1, 2);
        tournamentInProgress.setMatchResult(matchId, 10, 0);
    }

    private ArrayList<Team> getTeams() throws Throwable {
        TeamFactory teamFactory = new TeamFactory();
        ArrayList<Team> teams = new ArrayList<>();
        teams.add(
                teamFactory.create(
                        new TeamId(1),
                        "Team #1",
                        "Pele",
                        "Edwin van der Sar"
                )
        );
        teams.add(
                teamFactory.create(
                         new TeamId(2),
                        "Team #2",
                        "Alessandro Del Piero",
                        "Gianluigi Buffon"
                )
        );
        teams.add(
                teamFactory.create(
                        new TeamId(3),
                        "Team #3",
                        "Diego Maradona",
                        "David Seaman"
                )
        );
        teams.add(
                teamFactory.create(
                        new TeamId(4),
                        "Team #4",
                        "Zinedine Zidane",
                        "Peter Schmeichel"
                )
        );
        return teams;
    }

    private ArrayList<Team> getInvalidTeams() throws Throwable {
        TeamFactory teamFactory = new TeamFactory();
        ArrayList<Team> teams = new ArrayList<>();
        teams.add(
                teamFactory.create(
                        new TeamId(1),
                        "Team #1",
                        "Pele",
                        "Edwin van der Sar"
                )
        );
        teams.add(
                teamFactory.create(
                        new TeamId(1),
                        "Team #1",
                        "Alessandro Del Piero",
                        "Gianluigi Buffon"
                )
        );
        return teams;
    }

    private int[][] getResults(int index) {
        int[][][] results = {
                {
                        {4, 3},
                        {0, 0},
                        {1, 1},
                        {5, 5},
                        {2, 3},
                        {3, 3}
                },
                {
                        {6, 3},
                        {0, 0},
                        {1, 1},
                        {5, 5},
                        {2, 3},
                        {3, 3}
                }
        };
        return results[index];
    }

    @DataProvider
    public static Object[][] resultScenarios() {
        return new Object[][] {
                { 0, 4 },
                { 1, 1 },
        };
    }
}
